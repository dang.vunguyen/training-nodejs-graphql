const express = require('express');
const app = express();
const mongoose = require('mongoose');
const { graphqlHTTP } = require('express-graphql');
const { graphql } =require('graphql');
const logger = require('./core/logger');
const extensions = ({ context }) => {
    return {
        runtime: Date.now() - context.startTime,
    }
}
var jwt = require('jsonwebtoken');

app.listen(5000, async () => {
    console.log("server running ", 5000);
    await mongoose.connect('mongodb://localhost:27017/test_graphql', {
        useNewUrlParser: true,
        useUnifiedTopology: true,
    })
})

mongoose.connection.on('error', 
    console.error.bind(console, "MongoDB connection error: ")
)

const graphqlSchema = require('./schemas/index');

app.use(
    '/login',async (req, res, next) => {
        const token = jwt.sign({ id: req.headers.authorization }, 'secret');
        console.log(token);
        var decoded = jwt.verify(token, 'secret');
        const response2 = await graphql({
            schema: graphqlSchema,
            source: 'query($id: MongoID!) { userById(_id: $id) { _id name } }',
            variableValues: { id: decoded.id },
        });
        console.log(response2)
        next();
    }
);

app.use(
    '/graphql',
    graphqlHTTP((request) => {
        return {
            context: {startTime: Date.now()},
            graphiql: true,
            schema: graphqlSchema,
            extensions
        }
    }
))

