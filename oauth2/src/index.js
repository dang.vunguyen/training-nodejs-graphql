var express = require("express");
var axios = require("axios");
var cors = require("cors");
var cookieSession = require('cookie-session');
var passport = require('passport');
var GoogleStrategy = require('passport-google-oauth2').Strategy;
var FacebookStrategy = require('passport-facebook').Strategy;
passport.serializeUser(function (user, done) {
    done(null, user);
});
passport.deserializeUser(function (user, done) {
    done(null, user);
});
var GOOGLE_CLIENT_ID = "255548955171-k28spnpreqsnu7ofdqtl36m1dqhvhd94.apps.googleusercontent.com";
var GOOGLE_CLIENT_SECRET = "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";
var CLIENT_ID = "0d0a577b612e62859f19";
var CLIENT_SECRET = "5b389666e32dcd1ab42973bc83730c29f8a08967";
var GITHUB_URL = "https://github.com/login/oauth/access_token";
var FB_CLIENT_ID = "673383464076555";
var FB_CLIENT_SECRET = "ed669c1ed236f9f967d268a6594ea731";
var app = express();
app.use(cors({ credentials: true, origin: true }));
app.set('view engine', 'ejs');
app.use(express.static(__dirname + "/public"));
var access_token = "";
app.get('/', function (req, res) {
    res.render('index', { client_id: CLIENT_ID });
});
// GITHUB
app.get('/oauth/redirect', function (req, res) {
    axios({
        method: 'POST',
        url: "".concat(GITHUB_URL, "?client_id=").concat(CLIENT_ID, "&client_secret=").concat(CLIENT_SECRET, "&code=").concat(req.query.code),
        headers: {
            Accept: "application/json"
        }
    }).then(function (response) {
        access_token = response.data.access_token;
        res.redirect('/success');
    });
});
app.get('/success', function (req, res) {
    axios({
        method: 'get',
        url: "https://api.github.com/user",
        headers: {
            Authorization: 'token ' + access_token
        }
    }).then(function (response) {
        res.render('welcome', { userData: response.data });
    });
});
// GOOGLE
app.use(cookieSession({
    name: 'google-auth-session',
    keys: ['key1', 'key2']
}));
app.use(passport.initialize());
app.use(passport.session());
passport.use(new GoogleStrategy({
    clientID: GOOGLE_CLIENT_ID,
    clientSecret: GOOGLE_CLIENT_SECRET,
    callbackURL: "http://localhost:8080/auth/google/callback",
    passReqToCallback: true
}, function (request, accessToken, refreshToken, profile, done) {
    return done(null, profile);
}));
app.get('/auth/google', passport.authenticate('google', { scope: ['email', 'profile'] }));
app.get('/auth/google/callback', passport.authenticate('google', {
    failureRedirect: '/google/failed'
}), function (req, res) {
    res.redirect('/google/success');
});
app.get("/google/failed", function (req, res) {
    res.send("Failed");
});
app.get("/google/success", function (req, res) {
    res.send(req.user._json);
});
// FB
app.get("/auth/facebook", passport.authenticate("facebook"));
passport.use(new FacebookStrategy({
    clientID: FB_CLIENT_ID,
    clientSecret: FB_CLIENT_SECRET,
    callbackURL: 'http://localhost:8080/auth/facebook/callback',
    profileFields: ["email", "name"]
}, function (accessToken, refreshToken, profile, done) {
    done(null, profile);
}));
app.get("/auth/facebook/callback", passport.authenticate("facebook", {
    successRedirect: "/facebook/success",
    failureRedirect: "/facebook/failed"
}));
app.get("/facebook/failed", function (req, res) {
    res.send("Failed");
});
app.get("/facebook/success", function (req, res) {
    res.send(req.user);
});
var PORT = 8080;
app.listen(PORT, function () {
    console.log('listening on port ' + PORT);
});
