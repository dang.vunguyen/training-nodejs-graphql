const jwt = require('jsonwebtoken')
module.exports = (req, res, next) => {
    const token = req.headers['Authorization'] || req.headers['authorization'];
    if(!token) {
        req.isAuth = false;
        return next();
    }
    let decoded;
    try {
        decoded = jwt.verify(token, 'secret')
    } catch(err) {
        req.isAuth = false;
        return next();
    }
    if(!decoded){
        req.isAuth = false;
        return next();
    }
    req.isAuth = true;
    req.userId = decoded.userId;
    next();
}