const { ForbiddenError } = require("apollo-server-express");
// Middleware resolver
const isLoggedin = (parent, args, { req }, info) => {
  if (!req.isAuth) throw new ForbiddenError("Not Authorized");
};

const isAdmin = (parent, args, { admin }, info) => {
  if (admin !== "true") throw new ForbiddenError("Not Permited");
};


module.exports = { isLoggedin, isAdmin };