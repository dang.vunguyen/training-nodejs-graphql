const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const BookSchema = new Schema ({ 
    title: String,
    author: [String],
    cover: String,
    tag: [String]
},{
    timestamps: { createdAt: "create_at", updatedAt: "updated_at"}
})

module.exports = mongoose.model('books', BookSchema)
