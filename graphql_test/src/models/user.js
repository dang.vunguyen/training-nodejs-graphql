const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const User = new Schema ({
    name: String,
    email: String,
    phone: String,
    profileImage: String,
},{
    timestamps: { createdAt: "create_at", updatedAt: "updated_at"}
});

module.exports = mongoose.model('users', User)