const mongoose = require('mongoose');

mongoose.connect('mongodb://localhost:27017/test_graphql', {
        useNewUrlParser: true,
        useUnifiedTopology: true,
})

mongoose.connection.on('error', 
    console.error.bind(console, "MongoDB connection error: ")
)