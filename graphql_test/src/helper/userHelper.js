const User = require("../models/user")

module.exports = {
    allUser: User.find(),
    userById: function(id) {
        return User.findById(id)
    },
    userByEmail: function(email) {
        return User.findOne({email: email})
    }
}