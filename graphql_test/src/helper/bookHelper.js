const Book = require('../models/book')

module.exports = {
    allBook: Book.find(),
    bookById: function(id) {
        return Book.findById(id)
    }
}