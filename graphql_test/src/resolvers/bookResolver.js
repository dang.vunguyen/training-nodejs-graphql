const { ApolloError, ForbiddenError } = require("apollo-server-express");
const { combineResolvers } = require("graphql-resolvers");
const { isLoggedin, isAdmin } = require("../middlewares/validate");

const Book = require('../models/book')
const bookHelper = require('../helper/bookHelper')

module.exports = {
    Query: {
        books: combineResolvers(isLoggedin, () => {
            return bookHelper.allBook.then(books => {
                return books
            }).catch(err => {
                throw err
            })
        }),
        book: combineResolvers(isLoggedin, (_, { id }) =>{
            return bookHelper.bookById(id).then(book => {
                return book
            }).catch(err => {
                throw err
            })
        }),
    },
    Mutation: {
        createBook: combineResolvers(isLoggedin, (_, { title, author })=> {
            const book = new Book();
            book.title = title;
            book.author = author;
            return book.save()
        })
    }
}