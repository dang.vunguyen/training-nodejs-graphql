const { ApolloError, ForbiddenError } = require("apollo-server-express");
const jwt = require("jsonwebtoken");
const User = require('../models/user')
module.exports = {
    Query: {
        login: async (_, { email }) => {
            const user = await User.findOne({email: email})
            if(!user) { return new ForbiddenError('User not found') }
            const token = jwt.sign({userId: user.id, email: user.email}, 'secret', {expiresIn: '1h'})
            return {
                userId: user.id,
                token: token,
                tokenExpiration: 1
            }
        }
    }
}