const { ApolloError, ForbiddenError } = require("apollo-server-express");
const { combineResolvers } = require("graphql-resolvers");
const { isLoggedin, isAdmin } = require("../middlewares/validate");
const User = require("../models/user")
const userHelper = require("../helper/userHelper")

module.exports = {
    Query: {
      users: combineResolvers(isLoggedin, isAdmin, async () => {
        
        return userHelper.allUser.then(docs => {
          return docs
        }).catch(err =>{
          throw err
        })
      }),
      user: combineResolvers(isLoggedin, isAdmin, (_, { id }) => {
          return userHelper.userById(id).then(user =>{
              return user
          }).catch(err =>{
              throw err
          })
      }),
    },
    Mutation: {
        createUser: combineResolvers(isLoggedin, isAdmin, (_, { name }) => {
            const user = new User ({ name })
            return user.save()
        })
    },
  };