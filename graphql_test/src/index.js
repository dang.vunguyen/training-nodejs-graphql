const express = require('express');
const { ApolloServer } = require('apollo-server-express');
const isAuth = require('./middlewares/auth')

const indexTypeDefs = require('./schemas/indexSchema');
const indexResolver = require('./resolvers/indexResolver');
const userTypeDefs = require('./schemas/userSchema');
const userResolvers = require('./resolvers/userResolver');
const bookTypeDefs = require('./schemas/bookSchema');
const bookResolvers = require('./resolvers/bookResolver');

const app = express();
app.use(isAuth);
async function startServer() {
    const server = new ApolloServer({
        typeDefs: [indexTypeDefs, userTypeDefs, bookTypeDefs],
        resolvers: [indexResolver, userResolvers, bookResolvers],
        context: ({ req }) => {
            return {
              req: req,
              user: req.headers.user || "",
              admin: req.headers.admin || "false",
            };
        },
    });
    await server.start();
    server.applyMiddleware({ app });
    app.listen({ port: 4000 },async () => {
      console.log(`🚀 Server ready at http://localhost:4000${server.graphqlPath}`)
    }    
    );
}
startServer();
require('./db/db');