const { gql } = require('apollo-server-express');

module.exports = gql`
    type Query {
        login(email: String!): AuthData!
    }

    type AuthData {
        userId: ID!
        token: String!
        tokenExpiration: Int!
    }
`