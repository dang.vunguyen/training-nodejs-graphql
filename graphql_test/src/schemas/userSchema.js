const { gql } = require("apollo-server-express");
// Construct a schema, using GraphQL schema language
module.exports = gql`
directive @isLoggedin on FIELD_DEFINITION
directive @isAdmin on FIELD_DEFINITION

  type Query {
    users: [User]! @isLoggedin @isAdmin
    user(id: String!): User! @isLoggedin @isAdmin
  }

  type Mutation {
    createUser(name: String): User! @isLoggedin @isAdmin
  }

  type User {
    id: String!
    name: String!
    email: String!
    phone: String!
    profileImage: String!
  }
`;