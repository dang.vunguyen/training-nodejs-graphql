const { gql } = require("apollo-server-express");

module.exports = gql`
directive @isLoggedin on FIELD_DEFINITION

type Query {
    books: [Book]! @isLoggedin
    book(id: String!): Book! @isLoggedin
}

type Mutation {
    createBook(title: String!, author: [String]!): Book! @isLoggedin
}

type Book {
    id: String!
    title: String!
    author: [String]!
    cover: String!
    tags: [String]!
}

`