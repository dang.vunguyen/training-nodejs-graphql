const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const Tweet = new Schema({
    content: String,
    author: String,
})

module.exports = mongoose.model('tweets', Tweet);