const { gql } = require("apollo-server-express");
// Construct a schema, using GraphQL schema language
module.exports = gql`
directive @isLoggedin on FIELD_DEFINITION
directive @isAdmin on FIELD_DEFINITION

  type Query {
    hello: String
    tweets: [Tweet]! @isLoggedin @isAdmin
    tweet(id: Int!): Tweet! @isLoggedin @isAdmin
  }

  type Tweet {
    id: String!
    content: String!
    author: String!
  }
`;