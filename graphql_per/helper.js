const Tweet = require("./tweets")

module.exports = {
    findAllTweet: async (cb) => {
        await Tweet.find().exec((err, docs)=>{
            cb(null, docs);
        });
    }
}