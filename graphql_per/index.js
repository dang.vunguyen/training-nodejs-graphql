const express = require('express');
const { ApolloServer } = require('apollo-server-express');
const mongoose = require('mongoose');
// Construct a schema, using GraphQL schema language
const typeDefs = require('./schema');

// Provide resolver functions for your schema fields
const resolvers = require('./resolver');

const app = express();
async function startServer() {
    const server = new ApolloServer({
        typeDefs,
        resolvers,
        context: ({ req }) => {
            return {
              user: req.headers.user || "",
              admin: req.headers.admin || "false",
            };
        },
    });
    await server.start();
    server.applyMiddleware({ app });
    app.listen({ port: 4000 },async () => {
      console.log(`🚀 Server ready at http://localhost:4000${server.graphqlPath}`)
      await mongoose.connect('mongodb://localhost:27017/test_graphql', {
        useNewUrlParser: true,
        useUnifiedTopology: true,
      })
    }    
);
}
mongoose.connection.on('error', 
    console.error.bind(console, "MongoDB connection error: ")
)
startServer();

