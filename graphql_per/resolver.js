const { ApolloError, ForbiddenError } = require("apollo-server-express");
const tweets = require("./data");
const { combineResolvers } = require("graphql-resolvers");
const { isLoggedin, isAdmin } = require("./middlewares");
const helper = require('./helper')
const Tweet = require("./tweets")

// Provide resolver functions for your schema fields
module.exports = {
  Query: {
    hello: () => "Hello world!",
    tweets: combineResolvers(isLoggedin, isAdmin, async () => {
      return Tweet.find().then(docs => {
        return docs
      }).catch(err =>{
        throw err
      })
    }),
    tweet: combineResolvers(isLoggedin, isAdmin, (_, { id }) => {
        const tweetId = tweets.findIndex((tweet) => tweet.id === id);
        if (tweetId === -1) return new ApolloError("Tweet not found");
        return tweets[tweetId];
    }),
  },
};