"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.createGraphqlSchema = void 0;
const type_graphql_1 = require("type-graphql");
const userResolver_1 = require("../resolvers/userResolver");
const bookResolver_1 = require("../resolvers/bookResolver");
const indexResolver_1 = require("../resolvers/indexResolver");
const authChecker_1 = require("../middleware/authChecker");
const createGraphqlSchema = async () => {
    return await (0, type_graphql_1.buildSchema)({
        resolvers: [userResolver_1.UserResolver, bookResolver_1.BookResolver, indexResolver_1.IndexResolver],
        authChecker: authChecker_1.authChecker
    });
};
exports.createGraphqlSchema = createGraphqlSchema;
