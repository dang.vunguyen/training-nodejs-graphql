"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const typeorm_1 = require("typeorm");
const user_1 = require("../entity/user");
const book_1 = require("../entity/book");
exports.default = (0, typeorm_1.createConnection)({
    type: "mongodb",
    host: "localhost",
    port: 27017,
    database: "test_graphql",
    useUnifiedTopology: true,
    entities: [user_1.User, book_1.Book],
});
