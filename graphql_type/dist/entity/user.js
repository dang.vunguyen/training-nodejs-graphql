"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.UserUpload = exports.UserInput = exports.User = exports.Roles = void 0;
const typeorm_1 = require("typeorm");
const type_graphql_1 = require("type-graphql");
const baseEntity_1 = require("./baseEntity");
var Roles;
(function (Roles) {
    Roles["ADMIN"] = "ADMIN";
    Roles["CLIENT"] = "CLIENT";
})(Roles = exports.Roles || (exports.Roles = {}));
(0, type_graphql_1.registerEnumType)(Roles, {
    name: 'Roles',
    description: 'Roles of user',
});
let User = class User extends baseEntity_1.BaseEntity {
};
__decorate([
    (0, type_graphql_1.Field)(),
    (0, typeorm_1.Column)(),
    __metadata("design:type", String)
], User.prototype, "name", void 0);
__decorate([
    (0, type_graphql_1.Field)(),
    (0, typeorm_1.Column)({ unique: true }),
    __metadata("design:type", String)
], User.prototype, "email", void 0);
__decorate([
    (0, type_graphql_1.Field)(),
    (0, typeorm_1.Column)(),
    __metadata("design:type", String)
], User.prototype, "phone", void 0);
__decorate([
    (0, type_graphql_1.Field)(() => Roles),
    (0, typeorm_1.Column)({ default: Roles.CLIENT }),
    __metadata("design:type", String)
], User.prototype, "roles", void 0);
User = __decorate([
    (0, typeorm_1.Entity)('users'),
    (0, type_graphql_1.ObjectType)()
], User);
exports.User = User;
let UserInput = class UserInput {
};
__decorate([
    (0, type_graphql_1.Field)(),
    __metadata("design:type", String)
], UserInput.prototype, "name", void 0);
__decorate([
    (0, type_graphql_1.Field)(),
    __metadata("design:type", String)
], UserInput.prototype, "email", void 0);
__decorate([
    (0, type_graphql_1.Field)(),
    __metadata("design:type", String)
], UserInput.prototype, "phone", void 0);
__decorate([
    (0, type_graphql_1.Field)(),
    __metadata("design:type", String)
], UserInput.prototype, "roles", void 0);
UserInput = __decorate([
    (0, typeorm_1.Entity)('users'),
    (0, type_graphql_1.InputType)()
], UserInput);
exports.UserInput = UserInput;
let UserUpload = class UserUpload {
};
__decorate([
    (0, type_graphql_1.Field)(),
    __metadata("design:type", String)
], UserUpload.prototype, "id", void 0);
__decorate([
    (0, type_graphql_1.Field)(() => UserInput),
    __metadata("design:type", UserInput)
], UserUpload.prototype, "data", void 0);
UserUpload = __decorate([
    (0, typeorm_1.Entity)('users'),
    (0, type_graphql_1.ArgsType)()
], UserUpload);
exports.UserUpload = UserUpload;
