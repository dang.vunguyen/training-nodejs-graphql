"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.checkToken = void 0;
const jsonwebtoken_1 = __importDefault(require("jsonwebtoken"));
const checkToken = async (ureq, res, next) => {
    const token = ureq.header('Authorization') || ureq.header('authorization');
    const req = ureq;
    if (!token) {
        return next();
    }
    try {
        const decoded = jsonwebtoken_1.default.verify(token, 'secret');
        req.auth = decoded;
    }
    catch (err) {
        console.log('Auth error: ', err);
        return next();
    }
    next();
};
exports.checkToken = checkToken;
