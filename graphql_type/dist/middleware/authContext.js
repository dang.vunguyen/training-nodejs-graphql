"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.AuthedContext = void 0;
class AuthedContext {
    constructor(req, res) {
        this.req = req;
        this.res = res;
    }
}
exports.AuthedContext = AuthedContext;
