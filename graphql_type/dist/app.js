"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
require("reflect-metadata");
const express_1 = __importDefault(require("express"));
const app = (0, express_1.default)();
const apollo_server_express_1 = require("apollo-server-express");
const mongo_1 = __importDefault(require("./bootstrap/mongo"));
const buildSchema_1 = require("./bootstrap/buildSchema");
const auth_1 = require("./middleware/auth");
const authContext_1 = require("./middleware/authContext");
const { ApolloServerPluginLandingPageGraphQLPlayground } = require('apollo-server-core');
app.use(auth_1.checkToken);
async function startServer() {
    const schema = await (0, buildSchema_1.createGraphqlSchema)();
    await mongo_1.default;
    const server = new apollo_server_express_1.ApolloServer({
        schema,
        plugins: [
            ApolloServerPluginLandingPageGraphQLPlayground({
            // options
            })
        ],
        context: ({ req, res }) => new authContext_1.AuthedContext(req, res),
    });
    await server.start();
    server.applyMiddleware({ app, cors: true });
    app.listen({ port: 4000 }, async () => {
        console.log(`🚀 Server ready at http://localhost:4000${server.graphqlPath}`);
    });
}
startServer();
