"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.IndexResolver = void 0;
const typeorm_1 = require("typeorm");
const type_graphql_1 = require("type-graphql");
const user_1 = require("../entity/user");
const index_1 = require("../entity/index");
const jsonwebtoken_1 = __importDefault(require("jsonwebtoken"));
let IndexResolver = class IndexResolver {
    constructor() {
        this.userRepository = (0, typeorm_1.getMongoRepository)(user_1.User);
    }
    async login(email) {
        const user = await this.userRepository.findOne({ email: email });
        if (!user) {
            throw new Error("User not found");
        }
        let roles = 'CLIENT';
        if (user.roles) {
            roles = user.roles;
        }
        const token = jsonwebtoken_1.default.sign({ userId: user._id, email: user.email, roles: roles }, 'secret', { expiresIn: '1h' });
        return {
            userId: user._id,
            accessToken: token,
            roles: roles
        };
    }
};
__decorate([
    (0, type_graphql_1.Mutation)(() => index_1.AuthResponse),
    __param(0, (0, type_graphql_1.Arg)('email')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [String]),
    __metadata("design:returntype", Promise)
], IndexResolver.prototype, "login", null);
IndexResolver = __decorate([
    (0, type_graphql_1.Resolver)()
], IndexResolver);
exports.IndexResolver = IndexResolver;
