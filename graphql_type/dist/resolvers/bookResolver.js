"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.BookResolver = void 0;
const type_graphql_1 = require("type-graphql");
const book_1 = require("../entity/book");
const typeorm_1 = require("typeorm");
const authContext_1 = require("../middleware/authContext");
const user_1 = require("../entity/user");
let BookResolver = class BookResolver {
    constructor(bookRepository = (0, typeorm_1.getMongoRepository)(book_1.Book), userRepository = (0, typeorm_1.getMongoRepository)(user_1.User)) {
        this.bookRepository = bookRepository;
        this.userRepository = userRepository;
    }
    async getAll() {
        return await this.bookRepository.find();
    }
    async getById(id, ctx) {
        const book = await this.bookRepository.findOne(id);
        let checkLegit;
        book.author.forEach(author => {
            checkLegit = -1;
            if (author._id == ctx.req.auth.userId) {
                checkLegit = 1;
            }
        });
        if (checkLegit < 0) {
            throw new Error('Does not have permissions!');
        }
        else {
            return book;
        }
    }
    async bookPagi(first, offset) {
        const books = await this.bookRepository.find();
        let result = [];
        for (let i = 0; i <= books.length; i++) {
            if (i >= offset) {
                result.push(books[i]);
            }
            if (result.length === first) {
                break;
            }
        }
        return result;
    }
    async bookPagiCursor(first, cursor) {
        const books = await this.bookRepository.find();
        let after = 0;
        if (cursor != null) {
            let start = books.findIndex(book => book._id == cursor);
            if (start >= 0) {
                after = start + 1;
            }
        }
        let slice = books.slice(after, after + first);
        const promise = slice.map(async (book, index) => {
            const pr = book.author.map(async (author, index) => {
                const data = await this.userRepository.findOne(author._id);
                book.author[index] = data;
            });
            await Promise.all(pr);
        });
        await Promise.all(promise);
        return slice;
    }
    async bookPage(page) {
        const books = await this.bookRepository.find();
        const limit = 2;
        const pages = books.length / limit;
        return books.slice(limit * page - limit, limit * page);
    }
};
__decorate([
    (0, type_graphql_1.Query)(() => [book_1.Book]),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", []),
    __metadata("design:returntype", Promise)
], BookResolver.prototype, "getAll", null);
__decorate([
    (0, type_graphql_1.Authorized)(),
    (0, type_graphql_1.Query)(() => book_1.Book),
    __param(0, (0, type_graphql_1.Arg)("id")),
    __param(1, (0, type_graphql_1.Ctx)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [String, authContext_1.AuthedContext]),
    __metadata("design:returntype", Promise)
], BookResolver.prototype, "getById", null);
__decorate([
    (0, type_graphql_1.Query)(() => [book_1.Book]),
    __param(0, (0, type_graphql_1.Arg)("first")),
    __param(1, (0, type_graphql_1.Arg)("offset")),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Number, Number]),
    __metadata("design:returntype", Promise)
], BookResolver.prototype, "bookPagi", null);
__decorate([
    (0, type_graphql_1.Query)(() => [book_1.Book]),
    __param(0, (0, type_graphql_1.Arg)("first")),
    __param(1, (0, type_graphql_1.Arg)("cursor")),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Number, String]),
    __metadata("design:returntype", Promise)
], BookResolver.prototype, "bookPagiCursor", null);
__decorate([
    (0, type_graphql_1.Query)(() => [book_1.Book]),
    __param(0, (0, type_graphql_1.Arg)("page")),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Number]),
    __metadata("design:returntype", Promise)
], BookResolver.prototype, "bookPage", null);
BookResolver = __decorate([
    (0, type_graphql_1.Resolver)(of => book_1.Book),
    __metadata("design:paramtypes", [Object, Object])
], BookResolver);
exports.BookResolver = BookResolver;
