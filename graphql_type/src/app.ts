import "reflect-metadata";
import express from 'express';
const app = express();
import { ApolloServer } from 'apollo-server-express';
import createConnection from './bootstrap/mongo'
import {createGraphqlSchema} from './bootstrap/buildSchema'
import { checkToken } from './middleware/auth'
import { AuthedContext, AuthedRequest } from "./middleware/authContext";
const { ApolloServerPluginLandingPageGraphQLPlayground } = require('apollo-server-core');

app.use(checkToken);
async function startServer() {
    const schema = await createGraphqlSchema();
    await createConnection;
    const server = new ApolloServer({
        schema,
        plugins: [
          ApolloServerPluginLandingPageGraphQLPlayground({
            // options
          })
        ],
        context: ({ req, res }): AuthedContext => new AuthedContext(req as AuthedRequest, res),
    });
    await server.start();
    server.applyMiddleware({ app, cors: true });
    app.listen({ port: 4000 },async () => {
      console.log(`🚀 Server ready at http://localhost:4000${server.graphqlPath}`)
    }    
    );
}
startServer();