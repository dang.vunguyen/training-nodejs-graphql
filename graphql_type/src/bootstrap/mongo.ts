import {createConnection} from "typeorm";
import {User} from '../entity/user'
import {Book} from '../entity/book'
export default createConnection({
    type: "mongodb",
    host: "localhost",
    port: 27017,
    database: "test_graphql",
    useUnifiedTopology: true,
    entities: [User, Book],
});