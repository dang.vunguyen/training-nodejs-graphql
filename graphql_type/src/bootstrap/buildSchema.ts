import { buildSchema } from 'type-graphql';
import { GraphQLSchema } from "graphql";
import { UserResolver } from '../resolvers/userResolver'
import { BookResolver } from '../resolvers/bookResolver'
import { IndexResolver } from '../resolvers/indexResolver'
import { authChecker } from '../middleware/authChecker'

export const createGraphqlSchema = async (): Promise<GraphQLSchema> => {
    return await buildSchema({
      resolvers: [UserResolver, BookResolver, IndexResolver],
      authChecker: authChecker
    });
};
