import { Field, ObjectType } from 'type-graphql';

@ObjectType()
export class AuthResponse {

  @Field()
  userId!: string;
  
  @Field()
  accessToken!: string;

  @Field()
  roles! : string;
}
