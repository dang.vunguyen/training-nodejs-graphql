import { Field, ObjectType } from "type-graphql";
import { CreateDateColumn, Entity, PrimaryColumn, UpdateDateColumn } from "typeorm";


@Entity()
@ObjectType()
export class BaseEntity {
  @Field()
  @PrimaryColumn({ unique: true})
  _id!: string;

  @Field()
  @CreateDateColumn()
  createdAt!: Date;

  @Field()
  @UpdateDateColumn()
  updatedAt!: Date;
}
