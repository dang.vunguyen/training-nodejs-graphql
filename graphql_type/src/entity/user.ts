import { Entity, Column, PrimaryColumn } from 'typeorm';
import { ArgsType, Authorized, Field, InputType, ObjectType, registerEnumType} from 'type-graphql';
import { BaseEntity } from './baseEntity';

export enum Roles {
    ADMIN = 'ADMIN',
    CLIENT = 'CLIENT',
  }
registerEnumType(Roles, {
    name: 'Roles',
    description: 'Roles of user',
  });
  

@Entity('users')
@ObjectType()
export class User extends BaseEntity {
    @Field()
    @Column()
    name!: string;

    @Field()
    @Column({unique: true})
    email!: string;

    @Field()
    @Column()
    phone: string;

    @Field(() => Roles)
    @Column({default: Roles.CLIENT})
    roles: string;
}

@Entity('users')
@InputType()
export class UserInput implements Partial<User>{
    @Field()
    name: string;

    @Field()
    email: string;

    @Field()
    phone: string;

    @Field()
    roles: string;
}


@Entity('users')
@ArgsType()
export class UserUpload {
    @Field()
    id!: string;

    @Field(() => UserInput)
    data!: UserInput
}