import { Entity, Column, PrimaryColumn } from 'typeorm';
import { Arg, Field, ObjectType} from 'type-graphql';
import { User } from './user'
import { BaseEntity } from './baseEntity';

@ObjectType()
@Entity('books')
export class Book extends BaseEntity {

    @Field(() => [User])
    @Column()
    author: [User]

    @Field(() => [String])
    @Column()
    tag: [string]

    @Field()
    @Column()
    title: string

    @Field(() => [User])
    @Column()
    authorFind() {
        const authors = this.author
        if (!authors) throw new Error('Something went wrong!');
        return authors;
    }
}