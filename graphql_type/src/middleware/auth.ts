import jwt from 'jsonwebtoken'
import { Request, Response, NextFunction } from 'express';
import {AuthedRequest, AuthInfo} from './authContext'
    

export const checkToken = async (ureq: Request, res: Response, next: NextFunction): Promise<void> =>{
    const token = ureq.header('Authorization') || ureq.header('authorization');
    const req = ureq as unknown as AuthedRequest;
    if(!token) {
        return next();
    }
    try {
        const decoded = jwt.verify(token, 'secret') as AuthInfo;
        req.auth = decoded;
    } catch(err) {
        console.log('Auth error: ', err);
        return next();
    }
    next();
}