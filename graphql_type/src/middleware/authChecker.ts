import { AuthChecker } from 'type-graphql';
import { AuthedContext } from './authContext';

export const authChecker: AuthChecker<AuthedContext> = async ({ context }, roles) => {
    try {
        const auth = context.req.auth;
        if(!auth) return false;
        if (!roles || roles.length === 0) return true;
        const checkRole = roles.findIndex((role)=> role === auth.roles);
        if(checkRole < 0) return false;
        return true;
    } catch (err) {
        console.log(err);
        throw new Error('Token is not authenticated');
    }
}
