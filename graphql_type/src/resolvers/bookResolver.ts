import { Query, Arg, Resolver, Authorized, Ctx } from 'type-graphql';
import {Book} from '../entity/book'
import {getMongoRepository} from "typeorm";
import { AuthedContext } from '../middleware/authContext';
import { User } from '../entity/user';

@Resolver(of => Book)
export class BookResolver{
    constructor(
        private bookRepository = getMongoRepository(Book),
        private userRepository = getMongoRepository(User)
      ) {}
    
    @Query(() => [Book])
    async getAll(): Promise<Book[]> {
        return await this.bookRepository.find()
    }

    @Authorized()
    @Query(() => Book)
    async getById(@Arg("id") id: string, @Ctx() ctx: AuthedContext): Promise<Book> {
        const book = await this.bookRepository.findOne(id);
        let checkLegit;
        book.author.forEach(author => {
            checkLegit = -1;
            if(author._id == ctx.req.auth.userId) { 
                checkLegit = 1;
            }
        })
        if(checkLegit < 0) { 
            throw new Error('Does not have permissions!')
        } else {
            return book;
        }
        
    }

    @Query(() => [Book])
    async bookPagi(@Arg("first") first: number, @Arg("offset") offset: number): Promise<Book[]> {
        const books = await this.bookRepository.find();
        let result = [];
        
        for(let i = 0; i <= books.length; i++) {
            if(i >= offset) {
                result.push(books[i]);
            }

            if(result.length === first){
                break;
            }
        }

        return result
    }

    @Query(() => [Book])
    async bookPagiCursor(@Arg("first") first: number, @Arg("cursor") cursor: string): Promise<Book[]> {
        const books = await this.bookRepository.find();
        let after = 0;

        if(cursor != null){
            let start = books.findIndex(book => book._id == cursor);
            if(start >= 0){
                after = start + 1;
            }
        }
        
        let slice = books.slice(after, after + first);
        const promise = slice.map(async (book) => {
            const pr = book.author.map( async (author, index) => {
                const data = await this.userRepository.findOne(author._id);
                book.author[index] = data;
            })
            await Promise.all(pr)
        })
        
        await Promise.all(promise);
        return slice;
    }

    @Query(() => [Book])
    async bookPage(@Arg("page") page: number): Promise<Book[]> {
        const books = await this.bookRepository.find();
        const limit = 2;

        const pages = books.length / limit;

        return books.slice(limit*page - limit, limit*page);
    }
}