import { Query, Resolver, Arg, Mutation, Authorized, Args } from 'type-graphql';
import {User, UserInput, UserUpload, Roles} from '../entity/user'
import {getMongoRepository} from "typeorm";
@Resolver()
export class UserResolver {
    private userRepository = getMongoRepository(User)

    @Query(() => [User])
    async getAllUser(): Promise<User[]> {
        const users = this.userRepository.find();
        return users
    }

    @Authorized()
    @Query(() => User)
    async getOne(@Arg("id") id: string): Promise<User> {
        const user = await this.userRepository.findOne(id);
        if (user) return user;
        throw new Error('User not found');
    }

    @Mutation(() => User)
    async createUser(@Arg("data") data: UserInput): Promise<User>{
        const newUser = this.userRepository.create({
            ...data
        })
        await this.userRepository.insertOne(newUser);

        const user = await this.userRepository.findOne(
            {
                where: { 
                    email: newUser.email
                }
            }
        );

        return user;
    }

    @Mutation(() => User)
    async updateUser(@Args() {id, data}: UserUpload): Promise<User> {
        const userUpdate = await this.userRepository.update(id , 
            data
        );
        if(userUpdate.raw.matchedCount === 0) {
            throw new Error('User not found!')
        }
        return this.userRepository.findOne(id)
    }

    @Authorized(Roles.ADMIN)
    @Mutation(() => String)
    async removeUser(@Arg("id") id: string): Promise<String> {
        const userRemove = await this.userRepository.delete(id)
        if(userRemove.affected === 0) {
            throw new Error('User not found!')
        }
        return 'Remove successfully!'
    }
}