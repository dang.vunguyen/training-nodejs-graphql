import {getMongoRepository} from "typeorm";
import { Resolver, Arg, Mutation } from 'type-graphql';
import {User} from '../entity/user';
import { AuthResponse } from "../entity/index";
import jwt from 'jsonwebtoken';

@Resolver()
export class IndexResolver {
    private userRepository = getMongoRepository(User)

    @Mutation(() => AuthResponse)
    async login(@Arg('email') email: string): Promise<AuthResponse> {
        const user = await this.userRepository.findOne({ email: email });
        if(!user){ throw new Error("User not found"); }
        let roles = 'CLIENT'
        if(user.roles) { 
            roles = user.roles
        }
        const token:string = jwt.sign({userId: user._id, email: user.email, roles: roles}, 'secret', { expiresIn: '1h' });
        return {
            userId: user._id,
            accessToken: token,
            roles: roles
        }
    }
}